//import { createApp } from 'vue';
import { createApp,h } from 'VUE';
import { createRouter, createWebHashHistory } from 'vue-router';
import App from './App.vue'
import ProductPanel from './components/products/ProductPanel.vue'
import LoginPanel from './components/login-panel/LoginPanel.vue'
//Vue.config.productionTip = false

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    { path: '/', name: 'LoginPanel', component:LoginPanel },
    //{ path: '/productPanel', component: ProductPanel },
    { path: '/productPanel', name: 'ProductPanel', component: ProductPanel },

  ],

});

const app  = createApp({
    render: ()=>h(App)
});
app.use(router);
app.mount('#app');

